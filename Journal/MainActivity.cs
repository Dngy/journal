﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Journal.Helper;
using Android.Content;
using System.Collections.Generic;
namespace Journal
{
    [Activity(Label = "Journal", MainLauncher = true, Theme = "@style/Theme.AppCompat")]
    
    public class MainActivity : AppCompatActivity
    {
        EditText edtTask;
        DbHelper dbHelper;
        CustomAdapter adapter;
        ListView lstTask;
        
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return base.OnCreateOptionsMenu(menu);
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.action_add:
                    edtTask = new EditText(this);
                    Android.Support.V7.App.AlertDialog alertDialog = new Android.Support.V7.App.AlertDialog.Builder(this)
                        .SetTitle("Add New Task")
                        .SetMessage("What do you want to do next?")
                        .SetView(edtTask)
                        .SetPositiveButton("Add", OkAction)
                        .SetNegativeButton("Cancel", CancelAction)
                        .Create();
                    alertDialog.Show();
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
        private void CancelAction(object sender, DialogClickEventArgs e) //Aufgabe verwerfen
        {
        }
        private void OkAction(object sender, DialogClickEventArgs e) //Aufgabe hinzufügen
        {
            string task = edtTask.Text;
            dbHelper.InsertNewTask(task);
            LoadTaskList();
        }
        public void LoadTaskList() //Lade Daten
        {
            List<string> taskList = dbHelper.GetTaskList();
            adapter = new CustomAdapter(this, taskList, dbHelper);
            lstTask.Adapter = adapter;
        }
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //View vom main Layout setzen
            SetContentView(Resource.Layout.activity_main);
            dbHelper = new DbHelper(this);
            lstTask = FindViewById<ListView>(Resource.Id.lstTask);
            //Lade Daten 
            LoadTaskList();
        }
    }
}